// 23 Juni
let angka1 = prompt('Masukkan angka1');
let angka2 = prompt('Masukkan angka2');
let operator = prompt('Masukkan operator tambah, kurang, kali, bagi');
let isAgain;

if (Number(angka1) == NaN || Number(angka2) == NaN) {
    alert(`${angka1} atau ${angka2} bukan angka`);
} else {
    if (operator.toLowerCase() === 'tambah') {
        alert(Number(angka1) + Number(angka2));
    } else if (operator.toLowerCase() === 'kurang') {
        alert(Number(angka1) - Number(angka2));
    } else if (operator.toLowerCase() === 'kali') {
        alert(Number(angka1) * Number(angka2));
    } else if (operator.toLowerCase() === 'bagi') {
        alert(Number(angka1) / Number(angka2));
    } else {
        alert("Operator tidak ada");
        isAgain = confirm('Coba lagi?');
        if (isAgain === true) {
            location.reload();
        }
    }
}

