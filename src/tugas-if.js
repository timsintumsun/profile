//Pseudocode
// 1. Masukkan variable angka dalam prompt
// 2. if angka bukan nomor
//     2.1 kembali ke nomor point 1
//     2.2 jika angka berupa nomor, lanjut point 3
// 3. Modulo variable angka dengan 2
//     3.1 jika hasilnya 0, print genap
//     2.3 jika hasilnya bukan 0, print ganjil

let angka = prompt('Masukkan angka');
while (isNaN(Number(angka))) {
    angka = Number(prompt('Masukkan angka'));
}

if (angka === 0) {
    alert('ini adalah angka 0');
} else {
    if (angka % 2 === 0) {
        alert(`${angka} adalah bilangan genap`);
    } else {
        alert(`${angka} adalah bilangan ganjil`);
    }
}

// -------------------- TUGAS 2 --------------------
// Cara 1
let angka2 = prompt('Masukkan angka lagi');
while (isNaN(Number(angka2))) {
    angka2 = prompt('Masukkan angka lagi');
}
console.log(angka2);
if (Number(angka2) === 1) {
    alert('ini adalah angka satu');
} else if (Number(angka2) === 2) {
    alert('ini adalah angka dua');
} else if (Number(angka2) === 3) {
    alert('ini adalah angka tiga');
} else if (Number(angka2) === 4) {
    alert('ini adalah angka empat');
} else if (Number(angka2) === 5) {
    alert('ini adalah angka lima');
} else {
    alert('ini bukan cakupan angka di program!');
}

// Cara 2
let angka3 = prompt('Masukkan angka lagi');
while (isNaN(Number(angka3))) {
    angka3 = prompt('Masukkan angka lagi');
}

if (Number(angka3) > 0 && Number(angka3) < 6) {
    alert(`ini adalah angka ${angka3}`);
} else {
    alert('ini bukan cakupan angka di program!');
}

// -------------------- TUGAS 3 --------------------
// convert menjadi switch case

let angka4 = prompt('Masukkan angka lagi');
while (isNaN(Number(angka4))) {
    angka4 = prompt('Masukkan angka lagi');
}

switch (Number(angka4)) {
    case 1:
        alert('ini adalah angka satu');
        break;
    case 2:
        alert('ini adalah angka dua');
        break;
    case 3:
        alert('ini adalah angka tiga');
        break;
    case 4:
        alert('ini adalah angka empat');
        break;
    case 5:
        alert('ini adalah angka lima');
        break;
    default:
        alert('ini bukan cakupan angka di program!');
}
